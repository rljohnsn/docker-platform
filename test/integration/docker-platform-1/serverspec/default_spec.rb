#
# Copyright (c) 2016-2017 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
require 'common_docker'

hosts = [
  'docker-platform-1-centos-7',
  'docker-platform-2-centos-7',
  'docker-platform-3-centos-7'
]

def wait_initial_replicas_deployment
  (1..20).each do |try|
    cmd = 'docker service ls'
    result = `#{cmd} 2>&1`
    break if result.include?('5/5')
    puts "Waiting for deployment of replicas … Try ##{try}/30, waiting 5s"
    sleep(5)
  end
end

def wait_replicas_scaling(replicas_number)
  (1..20).each do |try|
    cmd = 'docker service ls'
    result = `#{cmd} 2>&1`
    break if result.include?("#{replicas_number}/#{replicas_number}")
    puts "Waiting for scaling of replicas … Try ##{try}/30, waiting 5s"
    sleep(5)
  end
end

def wait_replicas_balancing(server)
  (1..50).each do |try|
    cmd = "docker service ps redis -f node=#{server} -f desired-state=running"
    result = `#{cmd} 2>&1`
    break if result.include?(server)
    puts "Waiting for balancing of replicas … Try ##{try}/50, waiting 5s"
    sleep(5)
  end
end

def wait_redis_image
  (1..20).each do |try|
    cmd = 'docker images | grep "redis"'
    result = `#{cmd} 2>&1`
    break if result.include?('redis')
    puts "Waiting for redis image avaibility … Try ##{try}/30, waiting 5s"
    sleep(5)
  end
end

def replicas_scaling(replicas_number)
  cmd = "docker service scale redis=#{replicas_number}"
  `#{cmd} 2>&1`
end

describe 'Docker Image registry' do
  wait_redis_image
  it 'have redis image' do
    cmd = 'docker images | grep "redis"'
    result = `#{cmd} 2>&1`
    expect(result).to include('redis')
  end
end

describe 'Swarm cluster' do
  it 'is available and ready' do
    cmd = 'docker node ls'
    result = `#{cmd} 2>&1`
    expect(result).to include('docker-platform-1-centos-7')
    expect(result).to include('Ready')
    expect(result).to include('Active')
    expect(result).to include('Leader')
  end
end

describe 'Swarm services' do
  it 'is able to scale up the redis service to 8 replicas' do
    replicas_scaling(8)
    wait_replicas_scaling(8)
    cmd = 'docker service ls'
    result = `#{cmd} 2>&1`
    expect(result).to include('redis')
    expect(result).to include('8/8')
  end

  it 'is able to scale down the redis service to 8 replicas' do
    replicas_scaling(8)
    wait_replicas_scaling(8)
    cmd = 'docker service ls'
    result = `#{cmd} 2>&1`
    expect(result).to include('redis')
    expect(result).to include('8/8')
  end

  hosts.each do |server|
    it "have running replicas on #{server}" do
      wait_replicas_balancing(server)
      cmd =
        "docker service ps redis -f node=#{server} -f desired-state=running"
      result = `#{cmd} 2>&1`
      expect(result).to include(server)
    end
  end
end
